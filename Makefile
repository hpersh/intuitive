
CXXFLAGS = -std=c++11 -Wall -Werror
ifdef DEBUG
CXXFLAGS += -g
else
CXXFLAGS += -O2 -DNDEBUG
endif

SHELL =	/bin/bash

all:	test_temp_controller_fifo test_driver_fifo \
	test_temp_controller_socket test_driver_socket

temp_control.o: temp_control.cc temp_control.h logger.h alarms.h

temp_control_max_linear.o: \
	temp_control_max_linear.cc temp_control.h logger.h

logger.o: logger.cc logger.h

alarms.o: alarms.cc alarms.h logger.h

test_temp_controller.o: test_temp_controller.cc temp_control.h logger.h

test_temp_controller_fifo: \
	logger.o alarms.o temp_control.o temp_control_max_linear.o \
	ipc_fifo.cc test_temp_controller.o
	g++ $(CXXFLAGS) -o $@ $^

test_temp_controller_socket: \
	logger.o alarms.o temp_control.o temp_control_max_linear.o \
	ipc_socket.cc test_temp_controller.o
	g++ $(CXXFLAGS) -o $@ $^

test_driver_fifo:	test_driver_fifo.cc temp_control_ipc.h

test_driver_socket:	test_driver_socket.cc temp_control_ipc.h

.PHONY: doc check_fifo check_socket clean

doc:
	doxygen

check_fifo: test_temp_controller_fifo test_driver_fifo
	./run_test_fifo

check_socket: test_temp_controller_socket test_driver_socket
	./run_test_socket

clean:
	rm -fr *.o test*.out \
	test_temp_controller_{fifo,socket} test_driver_{fifo,socket} \
	latex html
