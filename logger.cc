/***************************************************************************

Simple logging implementation

Not really part of temperature controller implementation, but a facility
that it requires.  See README.

***************************************************************************/

#include <string.h>

#include <sstream>

#include "logger.h"

#define ARRAY_SIZE(a)  (sizeof(a) / sizeof((a)[0]))

// Create a logger

Logger::Logger(const char *prefix, Level level)
    : m_prefix (prefix), m_level (level)
{
}

// Log a message, with the given log level
// - If the given level is less than the current level,
//   the message is filtered out, i.e. not logged.

void Logger::log(Level level, std::string s)
{
    // Printable names for each log level
    static const char *level_names[] = {
        "DEBUG   ",
        "INFO    ",
        "WARNING ",
        "ERROR   ",
        "CRITICAL"
    };

    // Given level is less than current level => discard
    if (level < m_level)  return;

    // Compose message, with timestamp
    
    time_t now[1];
    time(now);
    char timestamp[26];
    ctime_r(now, timestamp);
    timestamp[strlen(timestamp) - 1] = 0;   // Clobber newline
    const char *level_name = (level >= ARRAY_SIZE(level_names))
        ? "*UKNOWN*"
        : level_names[level];
    std::stringstream mesg;
    mesg << timestamp << "|" << m_prefix << "|" << level_name << "|"
         << s << std::endl;

    // Write message to log
    write(mesg.str().c_str());
}

// Log a message

void Logger::log(Level level, const char *s)
{
    log(level, std::string(s));
}

// Set log level

void Logger::level_set(Level level)
{
    if (level >= LEVEL_MAX)  return;
    m_level = level;
}
