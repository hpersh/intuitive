# DESIGN CONSIDERATIONS

## General requirements for embedded systems

### Logging

A daemon must have a way to log its activity, in order to:
- record noteworthy events
- show its history of operation
- assist in diagnostics / debugging

Therefore, a simple logging service was created.  In a real system, logging would be a system-wide service, and this temperature control daemon would use that.

### Alarms

A daemon must have a way to record important events, in a centralized way, so that an operator, or other part of the system, may see the system status, and know that certain important events have occurred, and take remedial action, if applicable.

Therefore, a simple alarm service was created.  In a real system, alarm handling would be a system-wide service, and this temperature control daemon would use that.

## Application-specific requirements

### Operation of fans

Certain aspects of fan operation are not stated in the given specification, but should be implemented for more complete system protection:

- When the temperature controller first starts, it has no temperature information from the sensors.  To be conservative, all fans should start at 100%, and be brought down to a slower speed as temperature information is gathered.  This initial period of operation at 100% is expected to be very short, it would only last until the first sensor update is received.

- The daemon providing sensor readings to the IPC channel read by the temperature controller daemon should also pass updates indicating that a sensor has failed, e.g. the read operation to the hardware failed, the read operation returned a nonsense value, etc.  If the temperature controller has this information, it can know it has lost temperature information, and increase fans to 100%, for safety.

- Another possible cause of loss of sensor information is the failure of the daemon providing readings via the IPC channel.  The overall system governor should have a way to detect failed daemons, and restart them; however, it is still possible for any daemon to repeatedly fail, even after restarting.  In any case, for this application, it should wait for a reading on the IPC channel with a timeout, and if that timeout expires, declare that all temperature information is lost, raise a critical alarm, and set all fans to 100%.

- Just to note... the fan speed control algorithm given in the spec assumes that all sensors have the same "normal" band of temperature readings... which in real systems is not normally the case.  Some sensors are expected to be normally hotter than others.  For example, a sensor on the CPU die would run much hotter than a sensor monitoring air temperature -- this is to be expected.  In this case, the sensor with the highest operating range would always be sensor setting the maximum temperature value, thereby having all fans controlled by only one sensor, and any sensors with a lower normal operating band would have no effect on fan speed, even if it exceeds its normal operating temperature.

## Other design decisions

### Flexibility and extensiblility

- The given spec talks about subsystems (i.e. temperature sensors) and fans, resulting in 1 zone of control, i.e. all sensors control all fans.  It is trivial, and possibly valuable for (almost inevitable) future use, to implement multiple zones of control, i.e. a subsystem is a collection of sensors and fans, and each subsystem operates independently, possibly using its own fan speed control algorithm.

- Fan speed control algorithm parameters were given in the spec, but rather than hard code them into the body of the algorithm code, they are provided at run-time when the daemon is configured.

- Built-in support for different fan control algorithms, making adding a new algorithm easy.

### Visibility

Since the temperature controller shall be a closed and self-contained daemon, no attempt was made to implement data hiding.

## Assumptions

### Multiplicities and associations

1. There can be any number of subsystems.
2. A sensor belongs to exactly one subsystem.
3. A fan belongs to exactly one subsystem.

### Sensor uniqueness

Sensor identifiers are unique across the entire system.

### Static configuration once initialized

This is stated in the problem description, but just to reiterate all aspects of configuration...

The following are given at initializtion time, and will not change during
runtime:
- Number of subsystems
- Number of sensors per subsystem
- Number of fans per subsystem

### When to act

When a sensor update is received, action may need to be taken, namely, adjusting
the speed of one or more fans.

The converse is assumed to be true, namely, that when no update is
received, no action needs to be taken, and the temperature monitor may
idle (except for timeout case, see above).  For example, monitoring that the fans are actually running at the commanded speed is not required.  Nor is there a need to send a command to set the speed of a newly-inserted removable fan.

### Asynchronous temperature updates

Temperature updates are received via IPC, and may occur at any time.

### No loss of information

The IPC "channel" used to pass information to the temperature monitor is
assumed to lossless in nature, i.e. updates are queued in a FIFO when sent,
and read by the temperature controller.  Therefore, there is no need to
implement such queueing, in the case that an update arrives while already
processing an update, or multiple updates occuring at the same time,
in the temperature controller itself.

The FIFO is assumed to have sufficient capacity, given the worst-case sensor update
rates and sensor update processing times, that it shall never overflow.

### Duplicate/redundant information

The IPC "channel" may provide a redundant update, i.e. the update may have
a reading that is the same as the previous reading for the same sensor.

### No checking of timing of individual sensor updates

While there is a check for an overall sensor IPC timeout (see above), 
it is not the responsibility of the temperature monitor to detect that an
update has not been received for any one sensor for too long a time.

# REPO ORGANIZATION

| File | Description |
| --- | --- |
| alarms.h | Interface for simple alarm service |
| alarms.cc | Implemenation of simple alarm service |
| Doxyfile | Config file for doxygen |
| golden_{fifo,socket}.out | Expected output of test |
| ipc_{fifo,socket}.cc | IPC implementations |
| logger.h | Interface for simple logging service |
| logger.cc | Implementation of simple logging service |
| Makefile | Makefile |
| README.md | This file |
| run_test_{fifo,socket} | Script to run a test |
| temp_control.h | Data model for temperature control service |
| temp_control_ipc.h | Interface definition for IPC |
| temp_control.cc | Implementation for common portion of temperature control service |
| temp_control_max_linear.cc | Implementation for given max-linear temperature control algorithm |
| test_driver_{fifo,socket}.cc | Program to feed test sensor messages to test temperature controller |
| test_temp_controller.cc | Program to a test temperature controller, with a test configuration |


# HOW TO RUN A TEST

Run the command:
```
make check_fifo
```
to run a test using a FIFO for IPC.

Run the command:
```
make check_socket
```
to run a test using a Unix-domain socket for IPC.

The log generated by running a test is in the file "test.out".
