/***************************************************************************

Interface for Logger object

***************************************************************************/

#ifndef __LOGGER_H
#define __LOGGER_H

#include <string>

struct Logger {
    // Log levels, in decreasing order of verbosity
    enum Level {
          LEVEL_DEBUG,          // Debugging
          LEVEL_INFO,           // Informational
          LEVEL_WARN,           // Warning
          LEVEL_ERROR,          // Error
          LEVEL_CRITICAL,       // Critical error
          LEVEL_MAX
    };
    const char *m_prefix;       // Log message prefix
    Level      m_level;         // Current logging level

    // Create a logger
    Logger(
        const char *prefix,       // Prefix for every log message
        Level level = LEVEL_ERROR // Initial log level
           );

    // Set the current level for a logger
    // - Calls to log messages below this level (see levels above)
    //   will not be logged
    void level_set(Level level);

    // Log a message
    void log(Level level, std::string s);
    void log(Level level, const char *s);

    // External function, to write given string to the system log,
    // e.g. console and/or file and/or syslog, etc.
    static void write(const char *mesg);
};

#endif /* __LOGGER_H */
