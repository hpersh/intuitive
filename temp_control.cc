/***************************************************************************

Implementation for Temp_Control namespace

***************************************************************************/

#include <sstream>

#include "temp_control.h"
#include "logger.h"
#include "alarms.h"

// Map to lookup up sensor ids to instances of Sensor
// Note that sensor ids must be globally unique.
std::map<Temp_Control::Sensor::Id, Temp_Control::Sensor *>
    Temp_Control::Sensor::sensor_map;

// List of all configured subsystems
std::list<Temp_Control::Subsystem *> Temp_Control::Subsystem::list;

// Logger for temperature controller
Logger Temp_Control::logger("TEMPERATURE CONTROL");

// Create a sensor

void Temp_Control::Sensor::create(Id id, Temp_Control::Subsystem *subsys)
{
    Sensor *s = new Sensor();
    s->m_id     = id;
    s->m_state  = STATE_NOT_READ;
    s->m_subsys = subsys;

    // Add sensor to subsystem
    subsys->m_sensors.push_back(s);

    // Add sensor in lookup table
    sensor_map.insert(std::make_pair(id, s));
}

// Create a fan

void Temp_Control::Fan::create(
    Id        id,
    void      *hw_reg,
    unsigned  pwm_max,
    Subsystem *subsys
                               )
{
    Fan *f = new Fan();
    f->m_id      = id;
    f->m_hw_reg  = hw_reg;
    f->m_pwm_max = pwm_max;

    // Add fan to subsystem
    subsys->m_fans.push_back(f);
}

#define ARRAY_SIZE(a)  (sizeof(a) / sizeof((a)[0]))

// Set state of a sensor

void Temp_Control::Sensor::state_set(
    Temp_Control::Sensor::State       state,
    Temp_Control::Sensor::Temperature reading
                                     )
{
    if (m_state != state) {
        // Log state transition
        
        static const char *state_names[] = {
            "NOT_READ",
            "UP",
            "DOWN",
            "TIMED_OUT"
        };
        static const auto state_to_str =
            [](Temp_Control::Sensor::State state) -> const char *
            { 
                return (state >= ARRAY_SIZE(state_names)
                        ? "*UNKNOWN*"
                        : state_names[state]
                        );
            };
        std::stringstream log_mesg;
        log_mesg << "Sensor " << m_id << " state " << state_to_str(m_state)
                 << " -> " << state_to_str(state);
        Temp_Control::logger.log(Logger::LEVEL_DEBUG, log_mesg.str());
    }

    m_state       = state;
    m_reading     = reading;
    m_last_update = time(0);
}

// Set fan speed

void Temp_Control::Fan::speed_set(unsigned speed_pct)
{
    std::stringstream log_mesg;
    log_mesg << "Setting speed for fan " << m_id << " to "
             << speed_pct << "%";
    Temp_Control::logger.log(Logger::LEVEL_INFO, log_mesg.str());
        
    // Calculate PWM value
    // The spec says that the mapping from % of maximum fan speed
    // to PWM value is linear -- not all real fans work that way!
    // As long as max PWM value is reasonable, there is no danger
    // of arithmentic overflow in expression below.
    unsigned pwm = (speed_pct * m_pwm_max) / 100;

    log_mesg.str(std::string());
    log_mesg << "Writing 0x" << std::hex << pwm
             << " to address " << m_hw_reg;
    Temp_Control::logger.log(Logger::LEVEL_DEBUG, log_mesg.str());

    // Write PWM value to fan speed control register
    hw_reg_write(m_hw_reg, pwm);
}

// Set subsystem state

void Temp_Control::Subsystem::state_set(Temp_Control::Subsystem::State state)
{
    if (m_state != state) {
        // Log state transition
        
        static const char *state_names[] = {
            "MAX",
            "NORMAL"
        };
        static const auto state_to_str =
            [](Temp_Control::Subsystem::State state) -> const char *
            { 
                return (state >= ARRAY_SIZE(state_names)
                        ? "*UNKNOWN*"
                        : state_names[state]
                        );
            };
        std::stringstream log_mesg;
        log_mesg << "Subsystem " << m_id << " state " << state_to_str(m_state)
                 << " -> " << state_to_str(state);
        Temp_Control::logger.log(Logger::LEVEL_DEBUG, log_mesg.str());
    }

    m_state = state;
}

// Set speed for all fans in a subsystem

void Temp_Control::Subsystem::fan_speed_set(void)
{
    std::stringstream log_mesg;
    log_mesg << "Setting fan speed for subsystem " << m_id;
    Temp_Control::logger.log(Logger::LEVEL_INFO, log_mesg.str());

    // Check sensor states

    bool any_up = false, all_down = true;
    for (auto &s : m_sensors) {
        switch (s->m_state) {
        case Temp_Control::Sensor::STATE_UP:
            any_up   = true;
            all_down = false;
            break;
        case Temp_Control::Sensor::STATE_DOWN:
        case Temp_Control::Sensor::STATE_TIMED_OUT:
            break;
        default:
            all_down = false;
        }
    }

    // If and only if all sensors are down, raise critical alarm
    
    std::stringstream alarm_mesg;
    alarm_mesg << "Subsystem " << m_id << " sensor failure";
    std::string alarm_mesg_str = alarm_mesg.str();
    if (all_down) {
        Alarms::raise(Alarms::LEVEL_CRITICAL, alarm_mesg_str, time(0));
    } else {
        Alarms::clear(Alarms::LEVEL_CRITICAL, alarm_mesg_str);
    }

    // If no sensors have valid data, set all fans to 100%
    
    if (!any_up) {
        if (m_state != STATE_MAX) {
            state_set(Temp_Control::Subsystem::STATE_MAX);
            
            Temp_Control::logger.log(Logger::LEVEL_WARN,
                                     "No valid temperature reading"
                                     );
            for (auto &f : m_fans) {
                f->speed_set(100);
            }
        }
    } else {
        // At least one sensor is good
        
        state_set(Temp_Control::Subsystem::STATE_NORMAL);
    }
    
    algo();                     // Run specific algorithm
}

// Read a sensor update, and act on it

void sensor_update_get(void)
{
    // Blocking wait for an update on the IPC sensor update channel,
    // with timeout

    Temp_Control::Sensor::Id          id;
    Temp_Control::Sensor::Temperature reading;
    bool                              sensor_failure, read_err;
    Temp_Control::sensor_ipc_read(id, reading, sensor_failure, read_err);

    if (read_err) {
        // Timed out waiting for IPC read for sensor update

        Temp_Control::logger.log(Logger::LEVEL_ERROR,
                                 "Error reading sensor IPC"
                                 );

        // For each subsystem, ...
        for (auto &subsys : Temp_Control::Subsystem::list) {
            // Mark all sensors as invalid ...
            
            for (auto &sensor : subsys->m_sensors) {
                sensor->state_set(Temp_Control::Sensor::STATE_TIMED_OUT);
            }

            // ... and update fan speeds
            
            subsys->fan_speed_set();
        }

        return;
    }

    std::stringstream log_mesg;
    log_mesg << "Received sensor " << id << " reading " << reading
             << " sensor_failure " << sensor_failure;
    Temp_Control::logger.log(Logger::LEVEL_INFO, log_mesg.str());

    // Look up Sensor instance
    
    auto iter = Temp_Control::Sensor::sensor_map.find(id);
    if (iter == Temp_Control::Sensor::sensor_map.end()) {
        // Unknown sensor id => ignore update
        
        Temp_Control::logger.log(Logger::LEVEL_ERROR, "Unknown sensor");
        
        return;
    }
    Temp_Control::Sensor *s = iter->second;

    if (sensor_failure) {
        // Sensor update is notification of sensor failure

        // Mark sensor as invalid       
        s->state_set(Temp_Control::Sensor::STATE_DOWN, reading);

        log_mesg.str(std::string());
        log_mesg << "Sensor " << id << " failure";
        Temp_Control::logger.log(Logger::LEVEL_ERROR,
                                 log_mesg.str()
                                 );
        
        // Run fan speed update, with sensors that are known good
    } else {
        s->state_set(Temp_Control::Sensor::STATE_UP, reading);
    }

    // Run fan speed algorithm for owning subsystem
    
    s->m_subsys->fan_speed_set();
}

// Mainline

void Temp_Control::run(void)
{
    Temp_Control::logger.log(Logger::LEVEL_INFO, "Starting");

    // Start all fans at 100%
    
    for (auto &subsys : Temp_Control::Subsystem::list) {
        for (auto &fan : subsys->m_fans) {
            fan->speed_set(100);
        }
    }
    
    // Forever...
    for (;;) {
        // Read and process sensor updates
        
        sensor_update_get();
    }
}
