/***************************************************************************

Data model 

***************************************************************************/

#ifndef __TEMP_CONTROL_H
#define __TEMP_CONTROL_H

#include <time.h>

#include <string>
#include <list>
#include <map>

#include "logger.h"

namespace Temp_Control {
    struct Subsystem;

    //
    // Temperature sensor object
    //
    struct Sensor {
        typedef std::string Id;
        typedef float       Temperature;

        Id m_id;                // Unique sensor id
        enum State {
              STATE_NOT_READ = 0, // No information yet
              STATE_UP,           // Valid update received
              STATE_DOWN,         // Sensor failed
              STATE_TIMED_OUT     // Sensor update timeout
        } m_state;                 // Sensor state
        Temperature m_reading;     // Last sensor reading
        time_t      m_last_update; // When was updated last
        Subsystem   *m_subsys;     // Subsystem sensor is a part of

        // Set sensor state
        void state_set(State state, Temperature reading = 0.0);
        
        // Map sensor ids to instances
        static std::map<Id, Sensor *>  sensor_map;

        // Create a sensor, attached to the given subsytem
        static void create(Id id, Subsystem *subsys);
    };

    //
    // Fan object
    //
    struct Fan {
        typedef std::string Id;
        Id       m_id;        // Identifier
        void     *m_hw_reg;   // Hardware register to set speed (PWM)
        unsigned m_pwm_max;   // PWM count that corresponds to maximum
                              // fan speed (100% RPM)

        // Set fan speed, as % of maximum
        void speed_set(unsigned speed_pct);

        // Create a fan, attached to the given subsystem
        static void create(
            Id        id,       // Identifier
            void      *hw_reg,  // Hardware register for setting speed
            unsigned  pwm_max,  // PWM count for 100% speed
            Subsystem *subsys   // Subsystem fan belongs to
                           );
    };

    //
    // General subsystem object
    //
    struct Subsystem {
        typedef std::string Id;
        Id                  m_id;      // Identifier
        std::list<Sensor *> m_sensors; // Sensors in this subsystem
        std::list<Fan *>    m_fans;    // Fans controlled by this subsystem
        enum State {
            STATE_MAX = 0,      // All fans to 100%
            STATE_NORMAL        // Normal operation
        } m_state;              // Subsystem state
        time_t m_last_update; // When was updated last

        // Set speed for fans in this subsystem (common)
        void fan_speed_set(void);

        // Set subsystem state
        void state_set(State state);

        // Set speed for fans in this subsystem
        // - algorithm-specific, abstract
        virtual void algo(void) = 0;

        // List of all subsystems
        static std::list<Subsystem *> list;
    };

    //
    // Subsystem with fan speed control algorithm as clipped-linear
    // fan speed response to max sensor temperature
    //
    struct Subsystem_Max_Linear : Subsystem {
        // Fan speed control parameters
        // Let:
        //   M be the maximum reading for all sensors, and
        //   F be the fan speed for all fans
        // then:
        // if M <= THRESHOLD_LOWER, set F to SPEED_PCT_LOWER,
        // else if M >= THRESHOLD_UPPER, set F to SPEED_PCT_UPPER,
        // else set F to
        //                         (M - THRESHOLD_LOWER)
        // SPEED_PCT_LOWER + ----------------------------------- * (SPEED_PCT_UPPER - SPEED_PCT_LOWER)
        //                   (THRESHOLD_UPPER - THRESHOLD_LOWER)
        Sensor::Temperature m_thresh_lower, m_thresh_upper;
        unsigned            m_speed_pct_lower, m_speed_pct_upper;
        Sensor::Temperature m_slope;
        Sensor::Temperature m_max_reading;
        bool                m_max_valid;

        // Algorithm-specific set speed for fans in this subsystem
        void algo(void);

        // Create a subsystem, with algorithm parameters as above
        static Subsystem *create(
            Id                  id,
            Sensor::Temperature thresh_lower,
            Sensor::Temperature thresh_upper,
            unsigned            speed_pct_lower,
            unsigned            speed_pct_uppwer
                                 );
    };

    // Logger for temperature control
    extern Logger logger;

    // External function to read sensor updates from IPC stream
    extern void sensor_ipc_read(
        Temp_Control::Sensor::Id &sensor,  // Sensor that was read
        Sensor::Temperature      &reading, // Sensor reading
        bool                     &sensor_failure, // true <=> Sensor read failed
        bool                     &read_err // true <=> Error reading IPC
                                );

    // Begin reading sensor updates and setting fan speeds; never terminates
    void run(void);
};

// External function to perform write to hardware register
extern void hw_reg_write(void *addr, uint32_t value);

#endif /* __TEMP_CONTROL_H */
