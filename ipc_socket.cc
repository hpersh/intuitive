/***************************************************************************

One implementation of a sensor IPC channel, using FIFOs

***************************************************************************/

#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/un.h>
#include <signal.h>

#include <string>
#include <sstream>

#include "temp_control.h"
#include "temp_control_ipc.h"
#include "logger.h"

#define TIMEOUT     10             // Message wait timeout, in seconds

// Do-nothing alarm catcher functtion

static void alarm_catch(int dummy)
{
}

// Expects messages in the following format:
// - an ASCII comma-separated-value string (need not be null-terminated)
//   * the CSV string is of the form: <id>,<reading>,<status>
//     where:
//     <id>      is the sensor id (name),
//     <reading> is the temperature reading, as an ASCII string
//               representing a floating-point number
//     <status>  is the sensor status
//               0 => sensor is working
//               Non-zero => sensor has failed

void Temp_Control::sensor_ipc_read(
    Temp_Control::Sensor::Id          &sensor,
    Temp_Control::Sensor::Temperature &reading,
    bool                              &sensor_failure,
    bool                              &read_err
                                   )
{
    static bool init;           // One-time initialize flag
    static int  fd;             // File descriptor for FIFO

    if (!init) {
        // Initialize
        
        // Start with a fresh socket
        unlink(TEMP_CONTROL_SOCKET_FILENAME);

        // Create socket
        fd = socket(AF_UNIX, SOCK_DGRAM, 0);
        if (fd < 0) {
            Temp_Control::logger.log(Logger::LEVEL_ERROR,
                                     "Socket create failed"
                                     );
            Temp_Control::logger.log(Logger::LEVEL_ERROR, "Terminating");

            exit(0);
        }

        struct sockaddr_un sock_addr;
        if ((strlen(TEMP_CONTROL_SOCKET_FILENAME) + 1)
            > sizeof(sock_addr.sun_path)
            ) {
            Temp_Control::logger.log(Logger::LEVEL_ERROR,
                                     "Socket bind failed, filename too long"
                                     );
            Temp_Control::logger.log(Logger::LEVEL_ERROR, "Terminating");

            exit(0);
        }
        
        memset(&sock_addr, 0, sizeof(sock_addr));
        sock_addr.sun_family = AF_UNIX;
        strcpy(sock_addr.sun_path, TEMP_CONTROL_SOCKET_FILENAME);
        if (bind(fd,
                 (struct sockaddr *) &sock_addr,
                 sizeof(sock_addr)) < 0
            ) {
            Temp_Control::logger.log(Logger::LEVEL_ERROR,
                                     "Socket bind failed"
                                     );
            Temp_Control::logger.log(Logger::LEVEL_ERROR, "Terminating");

            exit(0);
        }
        
        // Catch alarm signals, and make them interrupt reads
        
        signal(SIGALRM, alarm_catch);
        siginterrupt(SIGALRM, 1);
        
        init = true;
    }

    for (;;) {
        alarm(TIMEOUT);         // Start timer for read timeout

        char mesg_buf[TEMP_CONTROL_MAX_MESG_LEN + 1];
        int n = recv(fd, mesg_buf, sizeof(mesg_buf), 0);
        if (n <= 0) {
            if (n == -1 && errno == EINTR) {
                Temp_Control::logger.log(Logger::LEVEL_ERROR,
                                         "Socket read timed out"
                                         );
            } else {
                Temp_Control::logger.log(Logger::LEVEL_ERROR,
                                         "Unexpected socket read error"
                                         );
            }
        
            alarm(0);                   // Cancel timeout

            read_err = true;

            return;
        }

        alarm(0);                   // Cancel timeout

        // Parse CSV values
    
        mesg_buf[n] = 0;
        char *p = mesg_buf;
        char *q = index(p, ',');
        if (q != 0) {
            *q = 0;
            sensor = std::string(p);
            p = q + 1;
            q = index(p, ',');
            if (q != 0) {
                *q = 0;
                sscanf(p, "%f", &reading);
                p = q + 1;
                int status;
                sscanf(p, "%d", &status);
                sensor_failure = (status != 0);
    
                read_err = false;

                return;
            }
        }

        // Message was not well-formed
    }
}
