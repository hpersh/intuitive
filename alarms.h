/***************************************************************************

Interface for simple alarm handling

***************************************************************************/

#ifndef __ALARMS_H
#define __ALARMS_H

#include <time.h>

#include <string>

namespace Alarms
{
    /* Alarm level */
    enum Level {
        LEVEL_MINOR,
        LEVEL_MAJOR,
        LEVEL_CRITICAL
    };

    /* Raise an alarm */
    void raise(Level level, std::string name, time_t when);
    void raise(Level level, const char *name, time_t when);

    /* Clear an alarm */
    void clear(Level level, std::string name);
    void clear(Level level, const char *name);
}

#endif /* __ALARMS_H */
