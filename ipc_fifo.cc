/***************************************************************************

One implementation of a sensor IPC channel, using FIFOs

***************************************************************************/

#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <signal.h>

#include <string>
#include <sstream>

#include "temp_control.h"
#include "temp_control_ipc.h"
#include "logger.h"

#define TIMEOUT     10             // Message wait timeout, in seconds
#define RETRY_TIME  10             // Time between read retries, in seconds

// Do-nothing alarm catcher functtion

static void alarm_catch(int dummy)
{
}

// Expects messages in the following format:
// - 1-byte message length (subsequent bytes, not including length byte)
// - an ASCII comma-separated-value string (need not be null-terminated)
//   * the CSV string is of the form: <id>,<reading>,<status>
//     where:
//     <id>      is the sensor id (name),
//     <reading> is the temperature reading, as an ASCII string
//               representing a floating-point number
//     <status>  is the sensor status
//               0 => sensor is working
//               Non-zero => sensor has failed
//
// Notes / Assumptions / Etc.
//
// 1. The message is assumed to be well-formed.
// 2. No attempt is made to resynchronize if a badly-formed message is
//    received.

void Temp_Control::sensor_ipc_read(
    Temp_Control::Sensor::Id          &sensor,
    Temp_Control::Sensor::Temperature &reading,
    bool                              &sensor_failure,
    bool                              &read_err
                                   )
{
    static bool init;           // One-time initialize flag
    static int  fd;             // File descriptor for FIFO
    static bool eof;            // True <=> last read was EOF

    if (!init) {
        // Initialize
        
        // Start with a fresh FIFO
        unlink(TEMP_CONTROL_FIFO_FILENAME);

        // Create and open FIFO
        
        if (mkfifo(TEMP_CONTROL_FIFO_FILENAME, 0622) < 0) {
            std::stringstream log_mesg;
            log_mesg << "FIFO create failed - " << strerror(errno);
            Temp_Control::logger.log(Logger::LEVEL_ERROR, log_mesg.str());
            Temp_Control::logger.log(Logger::LEVEL_ERROR, "Terminating");

            exit(0);
        }
        fd = open(TEMP_CONTROL_FIFO_FILENAME, O_RDONLY);
        if (fd < 0) {
            Temp_Control::logger.log(Logger::LEVEL_ERROR, "FIFO open failed");
            Temp_Control::logger.log(Logger::LEVEL_ERROR, "Terminating");

            exit(0);
        }
        
        // Catch alarm signals, and make them interrupt reads
        
        signal(SIGALRM, alarm_catch);
        siginterrupt(SIGALRM, 1);
        
        init = true;
    }

    if (eof) {
        sleep(RETRY_TIME);      // Wait to retry
        alarm(1);               // Start timer for read timeout
    } else {
        alarm(TIMEOUT);         // Start timer for read timeout
    }

    // Read message length
    
    unsigned char mesg_len;
    int n = read(fd, &mesg_len, 1);
    eof = false;
    if (n != 1) {
        if (n == 0) {
            Temp_Control::logger.log(Logger::LEVEL_ERROR, "FIFO read EOF");
            eof = true;
        } else if (n == -1 && errno == EINTR) {
            Temp_Control::logger.log(Logger::LEVEL_ERROR,
                                     "FIFO read timed out"
                                     );
        } else {
            Temp_Control::logger.log(Logger::LEVEL_ERROR,
                                     "FIFO unexpected read error"
                                     );
        }
        
        alarm(0);                   // Cancel timeout

        read_err = true;

        return;
    }

    // Read message body
    
    char mesg_buf[mesg_len + 1];
    n = read(fd, mesg_buf, mesg_len);
    if (n != mesg_len) {
        if (n == -1 && errno == EINTR) {
            Temp_Control::logger.log(Logger::LEVEL_ERROR,
                                     "FIFO read timed out"
                                     );
        } else {
            Temp_Control::logger.log(Logger::LEVEL_ERROR,
                                     "FIFO unexpected read error"
                                     );
        }
    
        alarm(0);                   // Cancel timeout

        read_err = true;

        return;
    }

    alarm(0);                   // Cancel timeout

    // Parse CSV values
    
    mesg_buf[mesg_len] = 0;
    char *p = mesg_buf;
    char *q = index(p, ',');
    *q = 0;
    sensor = std::string(p);
    p = q + 1;
    q = index(p, ',');
    *q = 0;
    sscanf(p, "%f", &reading);
    p = q + 1;
    int status;
    sscanf(p, "%d", &status);
    sensor_failure = (status != 0);
    
    read_err = false;
}
