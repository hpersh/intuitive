/***************************************************************************

Implementation for subsystem with fan speed control algorithm given
in spec, i.e. linear fan speed response, based on maximum sensor reading,
with upper and lower limits.

***************************************************************************/

#include <sstream>

#include "logger.h"
#include "temp_control.h"

// Create a subsystem

Temp_Control::Subsystem *Temp_Control::Subsystem_Max_Linear::create(
    Id       id,
    Temp_Control::Sensor::Temperature thresh_lower, 
    Temp_Control::Sensor::Temperature thresh_upper,
    unsigned speed_pct_lower,
    unsigned speed_pct_upper
                                                                    )
{
    Subsystem_Max_Linear *subsys = new Subsystem_Max_Linear();
    subsys->m_id = id;
    subsys->m_thresh_lower = thresh_lower;
    subsys->m_thresh_upper = thresh_upper;
    subsys->m_speed_pct_lower = speed_pct_lower;
    subsys->m_speed_pct_upper = speed_pct_upper;
    subsys->m_slope = (speed_pct_upper - speed_pct_lower) / (thresh_upper - thresh_lower);
    
    // Add to list of subsystems
    Temp_Control::Subsystem::list.push_back(subsys);
    
    return (subsys);
}

// Set speed for all fans in a subsystem

void Temp_Control::Subsystem_Max_Linear::algo(void)
{
    Temp_Control::logger.log(Logger::LEVEL_INFO, "Running max-linear algorithm");

    // Clear stored max temperature, if subsystem is in error state
    
    if (m_state == STATE_MAX) {
        m_max_valid = false;
        
        return;
    }

    // Find maximum sensor reading
    
    Temp_Control::Sensor::Temperature max = 0.0;
    bool max_valid = false;
    for (auto &s : m_sensors) {
        if (s->m_state != Temp_Control::Sensor::STATE_UP)  continue;
        if (!max_valid || s->m_reading > max) {
            max       = s->m_reading;
            max_valid = true;
        }
    }
    if (!max_valid) {
        Temp_Control::logger.log(Logger::LEVEL_ERROR, "Could not determine max reading");

        return;
    }

    std::stringstream log_mesg;
    log_mesg << "Maximum temperature is " << max;
    Temp_Control::logger.log(Logger::LEVEL_INFO, log_mesg.str());

    // If max reading is unchanged, no need to do anything
    
    if (m_max_valid && max == m_max_reading) {
        Temp_Control::logger.log(Logger::LEVEL_INFO, "Maximum temperature is unchanged");
        
        return;
    }

    // Record new max reading
    
    m_max_reading = max;
    m_max_valid   = true;
    m_last_update = time(0);
    
    // Compute fan speed (%)

    unsigned speed_pct;
    if (max <= m_thresh_lower) {
        speed_pct = m_speed_pct_lower;
    } else if (max >= m_thresh_upper) {
        speed_pct = m_speed_pct_upper;
    } else {
        speed_pct = m_speed_pct_lower + (unsigned)((max - m_thresh_lower) * m_slope);
    }

    log_mesg.str(std::string());
    log_mesg << "Setting all fan speeds to " << speed_pct << "%";
    Temp_Control::logger.log(Logger::LEVEL_INFO, log_mesg.str());
    
    // Set speed for all fans
    
    for (auto &f : m_fans) {
        f->speed_set(speed_pct);
    }
}

