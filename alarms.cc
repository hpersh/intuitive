/***************************************************************************

Simple alarms implementation

Not really part of temperature controller implementation, but a facility
that it requires.  See README.

***************************************************************************/

#include <map>

#include "alarms.h"
#include "logger.h"

static Logger logger("ALARMS             ", Logger::LEVEL_WARN);

// Map alarm level to log level

static Logger::Level level_to_logger_level(Alarms::Level level)
{
    switch (level) {
    case Alarms::LEVEL_MINOR:
        return (Logger::LEVEL_WARN);
    case Alarms::LEVEL_MAJOR:
        return (Logger::LEVEL_ERROR);
    case Alarms::LEVEL_CRITICAL:
        return (Logger::LEVEL_CRITICAL);
    default:
        ;
    }

    return (Logger::LEVEL_ERROR);
}

// Map to store outstanding (active) alarms
static std::map<std::string, time_t> alarms;

// Map an alarm (level and string name) to key for above map

#define ARRAY_SIZE(a)  (sizeof(a) / sizeof((a)[0]))

static void level_name_to_map_key(
    std::string   &key,
    Alarms::Level level,
    const char    *name
                                  )
{
    static const char *tbl[] = {
        "MINOR",
        "MAJOR",
        "CRITICAL"
    };

    key = std::string(level >= ARRAY_SIZE(tbl)
                      ? "*UKNOWN*"
                      : tbl[level]) + std::string(":") + std::string(name);
}

// Raise an alarm

void Alarms::raise(Level level, const char *name, time_t when)
{
    std::string key;
    level_name_to_map_key(key, level, name);

    // If alarm already outstanding, ignore
    if (alarms.find(key) != alarms.end())  return;
    
    alarms.insert(std::make_pair(key, when));
    logger.log(level_to_logger_level(level),
               std::string("Alarm set:") + key
               );
}

void Alarms::raise(Level level, std::string name, time_t when)
{
    Alarms::raise(level, name.c_str(), when);
}

// Clear an alarm

void Alarms::clear(Level level, const char *name)
{
    std::string key;
    level_name_to_map_key(key, level, name);

    // If alarm not outstanding, ignore
    if (alarms.find(key) == alarms.end())  return;
    
    alarms.erase(key);
    logger.log(level_to_logger_level(level),
               std::string("Alarm cleared:") + key
               );
}

void Alarms::clear(Level level, std::string name)
{
    Alarms::clear(level, name.c_str());
}
