/***************************************************************************

Temperature controller IPC interface

***************************************************************************/

#ifndef __TEMP_CONTROL_IPC_H
#define __TEMP_CONTROL_IPC_H

/* Filename for FIFO */
#define TEMP_CONTROL_FIFO_FILENAME  "/tmp/intuitive_temp_controller.fifo"

/* Filename for socket */
#define TEMP_CONTROL_SOCKET_FILENAME  "/tmp/intuitive_temp_controller.sock"

/* Maximum length of a sensor update message */
#define TEMP_CONTROL_MAX_MESG_LEN  40

#endif /* __TEMP_CONTROL_IPC_H */
