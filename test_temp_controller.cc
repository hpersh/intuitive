/***************************************************************************

Test harness for temperature controller

Creates a test configuration, and starts the temperature controller.

***************************************************************************/

#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <signal.h>

#include <string>
#include <iostream>

#include "logger.h"
#include "temp_control.h"

// Log write function, for testing

void Logger::write(const char *mesg)
{
    std::cout << mesg << std::flush;          // Print to stdout
    
}

// Set up subsystems, sensors and fans for test

void configure(void)
{
    Temp_Control::Subsystem *subsys
        = Temp_Control::Subsystem_Max_Linear::create(std::string("Chassis"),
                                                     25.0,
                                                     75.0,
                                                     20,
                                                     100
                                                     );

    Temp_Control::Sensor::create(std::string("CPU-internal"), subsys);
    Temp_Control::Sensor::create(std::string("CPU-external"), subsys);
    Temp_Control::Sensor::create(std::string("Mainboard"), subsys);
    Temp_Control::Sensor::create(std::string("Chassis-inlet"), subsys);
    Temp_Control::Sensor::create(std::string("Chassis-outlet"), subsys);
    
    Temp_Control::Fan::create(std::string("Chassis-left"),
                              (void *) 0x1000,
                              10000,
                              subsys
                              );
    Temp_Control::Fan::create(std::string("Chassis-mid"),
                              (void *) 0x1010,
                              10000,
                              subsys
                              );
    Temp_Control::Fan::create(std::string("Chassis-right"),
                              (void *) 0x1020,
                              10000,
                              subsys
                              );
    
    subsys = Temp_Control::Subsystem_Max_Linear::create(std::string("PSU1"),
                                                        25.0,
                                                        75.0,
                                                        20,
                                                        100
                                                        );
    
    Temp_Control::Sensor::create(std::string("PSU1-inlet"), subsys);
    Temp_Control::Sensor::create(std::string("PSU1-outlet"), subsys);
    
    Temp_Control::Fan::create(std::string("PSU-fan"),
                              (void *)
                              0x2000,
                              22000,
                              subsys
                              );
}

// For testing, do nothing for hardware writes

void hw_reg_write(void *addr, uint32_t value)
{
}

// Mainline, to run test

int main(void)
{
    // Create subsystems, sensors and fans for test
    
    configure();

    // Set temp controller for maximum debug output,
    // and start
    
    Temp_Control::logger.level_set(Logger::LEVEL_DEBUG);

    Temp_Control::run();
}
