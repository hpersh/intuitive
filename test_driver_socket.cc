/***************************************************************************

Test driver program

Feeds test sensor messages to the temperature controller FIFO.

***************************************************************************/

#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/un.h>
#include <string.h>

#include <iostream>

#include "temp_control_ipc.h"

#define ARRAY_SIZE(a)  (sizeof(a) / sizeof((a)[0]))

void test_mesgs_send(unsigned set_num)
{
    struct test_mesg {
        unsigned char len;
        const char    *mesg;
        unsigned      delay;
    };
#define INIT_MESG(s)  .len = sizeof(s) - 1, .mesg = (s)

    static const struct test_mesg test_mesgs1[] = {
        { INIT_MESG("Mainboard,29.0,0"),
          .delay = 1,
        },
        { INIT_MESG("PSU1-outlet,40.0,0"),
          .delay = 2
        },
        { INIT_MESG("XXX,0.0,0"),
          .delay = 1
        },
        { INIT_MESG("PSU1-inlet,999.0,1"),
          .delay = 5
        },
        { INIT_MESG("CPU-internal,70.0,0"),
          .delay = 1
        },
        { INIT_MESG("Mainboard,30.0,0"),
          .delay = 1
        },
        { INIT_MESG("CPU-internal,65.0,0"),
          .delay = 7
        },
        { INIT_MESG("PSU1-outlet,-999.0,1"),
          .delay = 1
        },
        { INIT_MESG("PSU1-outlet,40.0,0"),
          .delay = 15
        },
        { INIT_MESG("Mainboard,28.0,0") }
    };

    static const struct test_mesg test_mesgs2[] = {
        { INIT_MESG("Mainboard,33.0,0") }
    };
        
    int fd = socket(AF_UNIX, SOCK_DGRAM, 0);
    if (fd < 0) {
        std::cerr << "Test driver: Socket create failed - "
                  << strerror(errno) << std::endl;
        
        return;
    }

    struct sockaddr_un sock_addr;
    if ((strlen(TEMP_CONTROL_SOCKET_FILENAME) + 1)
        > sizeof(sock_addr.sun_path)
        ) {
        std::cerr << "Socket filename too long" << std::endl;
        
        return;
    }
        
    memset(&sock_addr, 0, sizeof(sock_addr));
    sock_addr.sun_family = AF_UNIX;
    strcpy(sock_addr.sun_path, TEMP_CONTROL_SOCKET_FILENAME);
    
    const struct test_mesg *t;
    unsigned n;
    if (set_num == 1) {
        t = test_mesgs1;
        n = ARRAY_SIZE(test_mesgs1);
    } else {
        t = test_mesgs2;
        n = ARRAY_SIZE(test_mesgs2);
    }
    
    unsigned i;
    for (i = 0; i < n; ++i) {
        if (t[i].len != 0) {
            std::cout << "Test driver: sending " << t[i].mesg << std::endl;
            if (sendto(fd,
                       t[i].mesg,
                       t[i].len,
                       0,
                       (struct sockaddr *) &sock_addr,
                       sizeof(sock_addr)) < 0
                ) {
                std::cerr << "Test driver: Failed to send message"
                          << std::endl;
            }
        }
        if (t[i].delay != 0) {
            std::cout << "Test driver: delaying " << t[i].delay << " seconds"
                      << std::endl;
            sleep(t[i].delay);
        }
    }

    close(fd);
}

// Mainline, to run test

int main(int argc, char **argv)
{
    if (argc != 2) {
        std::cerr << argv[0] << ": Expected set number" << std::endl;

        exit(1);
    }

    unsigned set_num;
    if (strcmp(argv[1], "1") == 0) {
        set_num = 1;
    } else  if (strcmp(argv[1], "2") == 0) {
        set_num = 2;
    } else {
        std::cerr << argv[0] << ": Expected set number 1 or 2" << std::endl;

        exit(1);
    }
    
    test_mesgs_send(set_num);
}
